package main

import (
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
)

func microserviceStatus(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "Auth microservice running")
}

func handleLogin(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "Nothing to see here")
}

func handleRegister(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "Nothing to see here")
}

func handleSendPassResetEmail(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "Nothing to see here")
}

func handlePasswordReset(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "Nothing to see here")
}

func main() {
	router := mux.NewRouter()

	router.HandleFunc("/", microserviceStatus).Methods("GET")
	router.HandleFunc("/login", handleLogin).Methods("POST")
	router.HandleFunc("/register", handleRegister).Methods("POST")
	router.HandleFunc("/reset/email", handleSendPassResetEmail).Methods("POST")
	router.HandleFunc("/reset/password", handlePasswordReset).Methods("POST")

	http.ListenAndServe(":8181", router)
}
